Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a
Changelog <https://keepachangelog.com/en/1.0.0/>`__, and this project
adheres to `Semantic
Versioning <https://semver.org/spec/v2.0.0.html>`__.

0.6.1_ - 2019-03-29
===================

Fixed
-----
- Better messages for errors

0.6.0_ - 2019-02-22
===================

Added
-----
- Keyboard shortcut ("F") for toggling the dot labels

0.5.0_ - 2019-02-22
===================

Added
-----
- Shift + Left Mouse for panning (for touchpads)

0.4.0_ - 2019-02-09
===================

Added
-----
- New option for showing labels for dots
- When switching bodyparts with the keyboard, the selected bodypart is displayed near the mouse cursor for convenience

Fixed
-----
- the slider widget for controlling dot size is now more responsive

0.3.0_ - 2019-02-04
===================

Added
-----
- Pan with middle mouse drag

Changed
-------
- Scrolling now zooms rather than vertically scrolling

Removed
-------
- Horizontal scrolling with Shift + Scroll

0.2.7_ - 2019-02-03
===================

Added
-----
- Unit testing, even for the GUI

Changed
-------
- Switched from pyyaml to ruamel.yaml
- Switched from native to non-native Qt file dialogs
- A lot of restructured code

Fixed
-----
- A lot of bugs, especially the bug where the scene wasn't being cleared so memory usage was steadily increase

0.2.2_ - 2019-01-27
===================

Added
-----
- Saving and loading a pickle file, just in case saving as a h5 file doesn't work

Fixed
-----
- Bug that prevented saving as a h5 file after a directory of frames was opened
- Bug that truncated the list of frames when opening a h5 file

0.2.0_ - 2019-01-27
===================

Added
-----
- Sphinx docs

Changed
-------
- Help dialog replaced with hyperlink to online docs
- Labels now have color icons rather than color backgrounds

0.1.1_ - 2019-01-26
===================

Added
-----
- More descriptive README with features list and screenshot
- this changelog

Fixed
-----
- Fix bug of loading images from the current directory

0.1.0 - 2019-01-25
==================

Added
-----
- Slider for selecting label dot size
- WASD bindings for navigating frames and bodyparts

0.0.3 - 2019-01-25
==================
Added
-----
- Main GUI from qt branch of UnicodeAlt255's fork of DeepLabCut

0.0.1 - 2019-01-25
==================

Added
-----
- Hatch init

.. _Unreleased: https://gitlab.com/d_/dlc-gui/compare/v0.6.1...master
.. _0.6.1: https://gitlab.com/d_/dlc-gui/compare/v0.6.0...v0.6.1
.. _0.6.0: https://gitlab.com/d_/dlc-gui/compare/v0.5.0...v0.6.0
.. _0.5.0: https://gitlab.com/d_/dlc-gui/compare/v0.4.0...v0.5.0
.. _0.4.0: https://gitlab.com/d_/dlc-gui/compare/v0.3.1...v0.4.0
.. _0.3.0: https://gitlab.com/d_/dlc-gui/compare/v0.2.7...v0.3.0
.. _0.2.7: https://gitlab.com/d_/dlc-gui/compare/v0.2.2...v0.2.7
.. _0.2.2: https://gitlab.com/d_/dlc-gui/compare/v0.2.0...v0.2.2
.. _0.2.0: https://gitlab.com/d_/dlc-gui/compare/v0.1.1...v0.2.0
.. _0.1.1: https://gitlab.com/d_/dlc-gui/compare/v0.1.0...v0.1.1
