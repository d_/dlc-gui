import sys

from tests.mock_project import ModelProject, ErroneousProject


mp = ModelProject()
ep = ErroneousProject()


def pytest_configure(config):
    sys._called_from_test = True


def pytest_unconfigure(config):
    del sys._called_from_test


def edit_config_inplace(config_path, key, new_value):
    import dlc_gui.util

    config_dict = dlc_gui.util.read_config_file(config_path)
    config_dict[key] = new_value
    dlc_gui.util.write_config_file(config_path, config_dict)


def pytest_sessionstart(session):
    """Run code before session.main()"""
    edit_config_inplace(mp.CONFIG_PATH, "project_path", mp.PROJECT_PATH)
    edit_config_inplace(ep.CONFIG_WRONG_TYPE_PROJECT_PATH_PATH, "project_path", None)
    edit_config_inplace(
        ep.CONFIG_NONEXISTENT_PROJECT_PATH_PATH, "project_path", "/sys/zxcvbnm"
    )


def pytest_sessionfinish(session, exitstatus):
    """Run code after test session"""
    edit_config_inplace(mp.CONFIG_PATH, "project_path", 0)
    edit_config_inplace(ep.CONFIG_WRONG_TYPE_PROJECT_PATH_PATH, "project_path", 0)
    edit_config_inplace(ep.CONFIG_NONEXISTENT_PROJECT_PATH_PATH, "project_path", 0)
