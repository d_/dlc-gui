from pathlib import Path
import sys


class ModelProject:
    """
    A minimal project with no errors
    """

    def __init__(self):
        self.THIS_DIR = Path(__file__).parent.resolve()
        self.PROJECT_PATH = self.THIS_DIR / "sample-Scorer-2019-01-27"
        self.CONFIG_PATH = self.PROJECT_PATH / "config.yaml"
        self.FRAMES_DIR = self.PROJECT_PATH / "labeled-data/kitten_stock_footage"
        self.H5_FILE_PATH = self.FRAMES_DIR / "CollectedData_Scorer.h5"
        self.PKL_FILE_PATH = self.FRAMES_DIR / "CollectedData_Scorer.pkl"
        self.CWD_PATH = Path.cwd()

        if sys.version_info < (3, 6):
            self.CONFIG_PATH = str(self.CONFIG_PATH)  # pragma: no cover


class ErroneousProject(ModelProject):
    """
    A minimal projects with various errors
    """

    def __init__(self):
        super(ErroneousProject, self).__init__()

        self.CONFIG_WRONG_TYPE_PROJECT_PATH_PATH = (
            self.PROJECT_PATH / "config_wrong_type_project_path.yaml"
        )
        self.CONFIG_NONEXISTENT_PROJECT_PATH_PATH = (
            self.PROJECT_PATH / "config_nonexistent_project_path.yaml"
        )

        self.H5_FILE_MISSING_GROUP_PATH = (
            self.FRAMES_DIR / "CollectedData_Scorer-missing_group_identifier.h5"
        )
        self.H5_FILE_MISSING_TABLE_PATH = (
            self.FRAMES_DIR / "CollectedData_Scorer-missing_table.h5"
        )
