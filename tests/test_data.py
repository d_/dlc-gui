from pathlib import Path

import numpy as np

import pytest

from hypothesis import given, settings, HealthCheck, reproduce_failure
from hypothesis import strategies as st

from tests.mock_project import ModelProject, ErroneousProject
import tests.strategies as tst

import dlc_gui.data
import dlc_gui.util

# TODO add malformed sample project to test error handling
# TODO edit in place all relative values in config.yaml
# TODO use mock monkeypatch for all relative values in config.yaml
# TODO use pytest tmpdir for generating temporary config.yaml


mp = ModelProject()
ep = ErroneousProject()


class TestErrors:
    """
    Make sure errors and weird inputs are handled gracefully
    """

    def test_init(self):
        with pytest.raises(TypeError):
            dlc_gui.data.DataModel(ep.CONFIG_WRONG_TYPE_PROJECT_PATH_PATH)
        with pytest.raises(FileNotFoundError):
            dlc_gui.data.DataModel(ep.CONFIG_NONEXISTENT_PROJECT_PATH_PATH)

    def test_init_from_file(self):
        dm = dlc_gui.data.DataModel(mp.CONFIG_PATH)
        assert dm.init_from_file(ep.H5_FILE_MISSING_GROUP_PATH) == 2
        dm = dlc_gui.data.DataModel(mp.CONFIG_PATH)
        assert dm.init_from_file(ep.H5_FILE_MISSING_TABLE_PATH) == 3

    @given(dir=tst.none_and_paths())
    @settings(deadline=None, suppress_health_check=[HealthCheck.too_slow])
    def test_init_from_dir(self, dir):
        dm = dlc_gui.data.DataModel(mp.CONFIG_PATH)
        # Main culprits: None, '.'
        if dir is None:
            assert dm.init_from_dir(dir) == 3
        else:
            if dm.project_path not in dir.parents:
                assert dm.init_from_dir(dir) == 2


class TestInitSymmetry:
    """
    Make sure that init_from_dir and init_from_file are fuctionally equivalent
    """

    dm_from_dir = dlc_gui.data.DataModel(mp.CONFIG_PATH)
    dm_from_dir.init_from_dir(mp.FRAMES_DIR)

    dm_from_file = dlc_gui.data.DataModel(mp.CONFIG_PATH)
    dm_from_file.init_from_file(mp.H5_FILE_PATH)

    def test_project_paths(self):
        assert mp.PROJECT_PATH == self.dm_from_dir.project_path
        assert mp.PROJECT_PATH == self.dm_from_file.project_path
        assert self.dm_from_dir.project_path == self.dm_from_file.project_path

    def test_paths_existence(self):
        assert mp.FRAMES_DIR.exists()
        assert mp.H5_FILE_PATH.exists()
        assert self.dm_from_dir.project_path.exists()
        assert self.dm_from_file.project_path.exists()

        for frame_name in self.dm_from_dir.frames_names:
            assert (mp.PROJECT_PATH / frame_name).exists()
        for frame_name in self.dm_from_file.frames_names:
            assert (mp.PROJECT_PATH / frame_name).exists()

        for frame_path in self.dm_from_dir.frames_paths:
            assert frame_path.exists()
        for frame_path in self.dm_from_file.frames_paths:
            assert frame_path.exists()

    def test_frames_paths(self):
        assert self.dm_from_dir.frames_paths == self.dm_from_file.frames_paths
        assert self.dm_from_dir.frames_names == self.dm_from_file.frames_names

    def test_frames_dict_len(self):
        assert len(self.dm_from_dir.frames_dict) == len(self.dm_from_file.frames_dict)

    def test_frames_dict(self):
        assert self.dm_from_dir.frames_dict == self.dm_from_file.frames_dict


class TestSaveOpenSymmetry:
    dm = dlc_gui.data.DataModel(mp.CONFIG_PATH)

    def test_hdf(self, tmpdir):
        self.dm.init_from_file(mp.H5_FILE_PATH)
        og_df = self.dm.data_frame

        tmp_hdf_path = Path(tmpdir, "tmp_hdf.h5")
        self.dm.save_as_hdf(tmp_hdf_path)

        self.dm.init_from_file(tmp_hdf_path)
        tmp_df = self.dm.data_frame

        assert og_df.equals(tmp_df)

    def test_pkl(self, tmpdir):
        self.dm.init_from_file(mp.PKL_FILE_PATH)
        og_df = self.dm.data_frame

        tmp_pkl_path = Path(tmpdir, "tmp_pkl.pkl")
        self.dm.save_as_pkl(tmp_pkl_path)

        self.dm.init_from_file(tmp_pkl_path)
        tmp_df = self.dm.data_frame

        assert og_df.equals(tmp_df)


class TestDataFrame:
    # make sure each x coordinate has a y and vice-versa
    dm = dlc_gui.data.DataModel(mp.CONFIG_PATH)
    dm.init_from_file(mp.H5_FILE_PATH)
    df = dm.data_frame
    scorer = dm.scorer
    frames = dm.frames_names
    bodyparts = dm.bodyparts

    def test_coords(self):
        for frame in self.frames:
            for bodypart in self.bodyparts:
                x, y = self.dm.get_coords_from_dataframe(str(frame), bodypart)
                assert x == y

    @given(
        coords=st.tuples(st.floats(), st.floats()),
        slice=st.integers(min_value=0, max_value=len(bodyparts)),
    )
    @settings(max_examples=30, deadline=None)
    def test_get_add_symmetry(self, coords, slice):

        for frame in self.frames[slice - 3 : slice]:
            for bodypart in self.bodyparts[slice - 3 : slice]:
                self.dm.add_coords_to_dataframe(str(frame), bodypart, coords)

        for frame in self.frames[slice - 3 : slice]:
            for bodypart in self.bodyparts[slice - 3 : slice]:
                read_coords = self.dm.get_coords_from_dataframe(str(frame), bodypart)
                coords = tuple(np.nan if i is None else i for i in coords)
                coords = tuple(None if np.isnan(i) else i for i in coords)
                # print(f"{frame} {bodypart} - wrote: {coords} | read: {read_coords}")
                assert read_coords == coords
